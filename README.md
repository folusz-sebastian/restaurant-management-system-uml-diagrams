# UML modelling for restaurant management system

##  Opis projektu
W ramach projektu zostały wytworzone modele oprogramowania wspierającego funkcjonowanie restauracji. Projekt był tworzony zgodnie z metodyką obiektową.

Zostały utworzone następujące modele:

- model biznesowych przypadków użycia
    - Diagram biznesowych przypadków użycia
    - diagramy sekwencji
- model analityczny organizacji,
- model przypadków użycia,
- model analityczny systemu informatycznego,
    - wstępny model konstrukcyjny,
    - diagram realizacji przypadków użycia,
    - diagramy sekwencji,
    - model statyczny klas analitycznych
- diagram procesów,
- diagram implementacji procesów,
- model montażowy